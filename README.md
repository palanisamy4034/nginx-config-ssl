
## nginx folder structure
```sh 
mkdir -p /usr/local/etc/nginx/sites-{enabled,available}
cd /usr/local/etc/nginx/sites-enabled
ln -s ../sites-available/default.conf
ln -s ../sites-available/default-ssl.conf
```


# `File locations:`

- nginx.conf to /usr/local/etc/nginx/
- default.conf and default-ssl.conf to /usr/local/etc/nginx/sites-available
- homebrew.mxcl.nginx.plist to /Library/LaunchDaemons/



# `Password`

```sh
$ sudo apt-get install apache2-utils
$ sudo htpasswd -c /etc/nginx/.htpasswd nginx
$ cat /etc/nginx/.htpasswd
$ sudo vi /etc/nginx/sites-available/default 
```

## password Config

```sh
auth_basic "Private Property";
auth_basic_user_file /etc/nginx/.htpasswd;
```



